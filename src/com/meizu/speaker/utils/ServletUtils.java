package com.meizu.speaker.utils;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public final class ServletUtils {
	public static Boolean USE_MYSQL_DB = true;
	
	public static void attachJsonResultToResponse(ServletResponse resp, String jsonResult, String jsonCallback){
		try {
//			resp.setHeader("content-type", "text/html;charset=UTF-8");
			resp.setCharacterEncoding("UTF-8");
			resp.setContentType("text/html;charset=UTF-8");
			if(jsonCallback != null) {
				jsonResult = jsonCallback + "(" + jsonResult + ")";
			}
			
			System.out.println("attachJsonResultToResponse jsonResult:" + jsonResult);
		    OutputStream out = resp.getOutputStream();	
		    out.write(jsonResult.getBytes("UTF-8"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	    	System.out.println("Exception:" + e);
		}
	}
	
	
	
	

}
