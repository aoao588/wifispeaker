package com.meizu.speaker.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

/**
 * 
 * @author wangliao
 * 保存升级配置信息的单例类
 *
 */
public class UpgCfgHelper {
	private String testUpgCfgInfo;
	
	private String stableUpgCfgInfo;
	
	private static UpgCfgHelper mInstance;
	
	private UpgCfgHelper(){
		
	}
	
	public static UpgCfgHelper getInstance(){
		if(mInstance == null){
			synchronized(UpgCfgHelper.class){
				if(mInstance == null){
					mInstance = new UpgCfgHelper();
					return mInstance;
				}
				
			}
		}
		return mInstance;
	}
	
	public String getUpgCfgInfo(int release) {
		synchronized(UpgCfgHelper.class){
			String cfgFilePath;
			if(release == 0) {
				if(testUpgCfgInfo != null) {
					return testUpgCfgInfo;
				}
				cfgFilePath = "/mnt/WifiSpeakerUpg/testUpgCfg.txt";
			} else {
				if(stableUpgCfgInfo != null) {
					return stableUpgCfgInfo;
				}
				cfgFilePath = "/mnt/WifiSpeakerUpg/stableUpgCfg.txt";
			}
			
			System.out.println("----getUpgCfgInfo：" + cfgFilePath);
			
			try {
				StringBuilder sb = new StringBuilder();
				BufferedReader br = new BufferedReader(new FileReader(cfgFilePath));
				String str = null;
				while ((str = br.readLine()) != null) {
					System.out.println(str);
					sb.append((str != null && !("".equals(str)))? str : "");
				}
				
				return sb.toString();
			} catch(Exception e) {
				e.printStackTrace();
			}
			
			return "";
		}
	}
	
	public boolean setUpgCfgInfo(String info, int release) {
		synchronized(UpgCfgHelper.class){
			String cfgFilePath;
			if(release == 0) {
				cfgFilePath = "/mnt/WifiSpeakerUpg/testUpgCfg.txt";
			} else {
				cfgFilePath = "/mnt/WifiSpeakerUpg/stableUpgCfg.txt";
			}
			
			System.out.println("----setUpgCfgInfo：" + cfgFilePath);
			
			try {
				File cfgText = new File(cfgFilePath);
				if(!cfgText.exists()){
					//文件不存在
					cfgText.createNewFile();
				}
				//找到文件，以覆盖的方式去重写txt文件
				FileWriter fw = new FileWriter(cfgText);
				//清空txt文件的内容
				fw.write("");
				//重写
				fw.write(info);
				fw.close();
				
				if(release == 0) {
					this.testUpgCfgInfo = info;
				} else {
					this.stableUpgCfgInfo = info;
				}
				
				return true;
			} catch(Exception e) {
				e.printStackTrace();
			}
			
			return false;
		}
	}
}
