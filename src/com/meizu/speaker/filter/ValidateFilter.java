package com.meizu.speaker.filter;
import java.io.IOException;

//import javax.imageio.spi.ServiceRegistry.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.meizu.speaker.utils.ServletUtils;

/**
 * 
 * @author wangliao
 * 防止用户直接获取jsp资源的过滤器
 *
 */
public class ValidateFilter implements Filter {
	private FilterConfig filterConfig;

	@Override
	public void destroy() {	
	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) arg0;
		HttpServletResponse resp = (HttpServletResponse) arg1;
		String request_uri = req.getRequestURI();
		String ctx_path = req.getContextPath();
		
		int ctxPathLength = ctx_path.length();
		String action = request_uri.substring(ctx_path.length());
		String lastCh = action.substring(action.length() - 1);
		//Not allow to access folder and some resource files directly
		if(lastCh.equals("/")
			|| action.contains("unaccessible")) {
			resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			ServletUtils.attachJsonResultToResponse(resp, "Not found!", req.getParameter("jsoncallback"));
		}else{
			arg2.doFilter(arg0, arg1);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		filterConfig = arg0;	
	}
}
