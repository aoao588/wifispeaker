package com.meizu.speaker.weixin;
import java.util.TimerTask;

import net.sf.json.JSONObject;

/**
 * 获取token和ticket
 * @author wangliao
 *
 */
public class NewTimerTask extends TimerTask{
	private String  m_token;
	private String m_jsTicket;
	private String accessTokenUrl;
	private String ticketUrl;
	private HttpConnection httpConnection;
	
	public  NewTimerTask(String accessTokenUrl,String ticketUrl){
		this.accessTokenUrl = accessTokenUrl;
		this.ticketUrl = ticketUrl;
		httpConnection = new HttpConnection();
	}
	
	
	public String getM_token() {
		return m_token;
	}


	public String getM_jsTicket() {
		return m_jsTicket;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			getTokenFromWechat();
			getJsTicketFromWechat();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
    private  void  getJsTicketFromWechat() throws Exception{ 
	
		if(m_token != null){
		
			System.out.println("token不为空 "+m_token);
		}
        String responeText = httpConnection.get(ticketUrl+m_token);
        JSONObject obj = JSONObject.fromObject(responeText);
        String jsTicket = obj.getString("ticket");
        m_jsTicket = jsTicket;
  
    }

    private void getTokenFromWechat() throws Exception{
	String responseText = httpConnection.get(accessTokenUrl);
	JSONObject obj = JSONObject.fromObject(responseText);
	String token  = obj.getString("access_token");
	m_token = token;

}

}
