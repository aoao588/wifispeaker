package com.meizu.speaker.weixin;
import net.sf.json.JSONObject;

/**
 * 定时获取token和ticket的类，防止访问频繁更新卡券
 * 单例对象，不能重复创建
 * @author wangliao
 *
 */
public class FetchToken {
	//有效期为7200s
	private long getTokenTime;
	private String accessTokenUrl;
	private long systemTime;
	private HttpConnection httpConnection;
	private String validToken;
	
	private static FetchToken mInstance;
	
	private FetchToken(long systemTime,String accessTokenUrl){
		this.accessTokenUrl = accessTokenUrl;
		this.systemTime = systemTime;
		httpConnection = new HttpConnection();
		//initialize
		initialToken();
	}

	public static FetchToken getInstance(long systemTime,String accessTokenUrl){
		if (mInstance == null){
			synchronized (FetchToken.class) {
				if (mInstance == null){
					mInstance = new FetchToken(systemTime,accessTokenUrl);
				}
				
			}
		}
		//update the systemTime
		mInstance.setSystemTime(systemTime);
		return mInstance;
	}
	
	//getters and setters
	public long getGetTokenTime() {
		return getTokenTime;
	}

	public void setGetTokenTime(long getTokenTime) {
		this.getTokenTime = getTokenTime;
	}

	public String getAccessTokenUrl() {
		return accessTokenUrl;
	}

	public void setAccessTokenUrl(String accessTokenUrl) {
		this.accessTokenUrl = accessTokenUrl;
	}

	public long getSystemTime() {
		return systemTime;
	}

	public void setSystemTime(long systemTime) {
		this.systemTime = systemTime;
	}
	
	

	public String getValidToken() {
		return validToken;
	}

	public void setValidToken(String validToken) {
		this.validToken = validToken;
	}

	//compare systemTime with getTokenTime
	public boolean compare(){
		if((systemTime-getTokenTime) / 1000 < 7200){
			//token isn't expired
			return false;
		}else{
			//token expired
			return true;
		}
	}
	
	public String  getTokenFromWeixin() throws Exception{
		if(accessTokenUrl != null && compare()){
			String responseText = httpConnection.get(accessTokenUrl);
			JSONObject obj = JSONObject.fromObject(responseText);
			String token  = obj.getString("access_token");
			//update the getTokenTime
			setGetTokenTime(System.currentTimeMillis());
			//update the validToken
			setValidToken(token);
			return token;
		}else{
		     return validToken;
		}
	}
	
	private void initialToken(){
		//invoked only once in the construction 
		try{
		String responseText = httpConnection.get(accessTokenUrl);
		JSONObject obj = JSONObject.fromObject(responseText);
		String token  = obj.getString("access_token");
		setGetTokenTime(System.currentTimeMillis());
		setValidToken(token);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

}
