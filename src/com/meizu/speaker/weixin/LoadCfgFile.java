package com.meizu.speaker.weixin;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.meizu.speaker.utils.ServletUtils;

@WebServlet("/LoadCfgFile")
public class LoadCfgFile extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//确定文件类型，根据mime标准去写
		//普通文本
		String fileName = request.getParameter("filename");
		ServletContext ctx = this.getServletContext();
		//获取资源
		File fileLoad = new File("/mnt/WifiSpeakerUpgBin/"+fileName);
		if(!fileLoad.exists()){
			//该文件名不存在
			ServletUtils.attachJsonResultToResponse(response, "No such file exists",request.getParameter("jsoncallback"));
			return;
		}else{
			response.setHeader("content-type", "application/octet-stream");
			//以弹框的形式，并设置文件名
			response.addHeader("content-disposition", "attachment;filename=config.rar");
			FileInputStream is = new FileInputStream(fileLoad);
			long size = fileLoad.length();
			response.addHeader("Content_Length", String.valueOf(size));
			int read = 0;
			byte[] bytes = new byte[500];
			OutputStream os = response.getOutputStream();
	        while((read = is.read(bytes)) != -1) {
	            os.write(bytes, 0, read);
	        }
	        os.flush();
	        os.close();
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}

}
