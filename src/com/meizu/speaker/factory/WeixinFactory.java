package com.meizu.speaker.factory;

import java.util.UUID;
import com.meizu.speaker.weixin.HttpConnection;
import com.meizu.speaker.weixin.JsAPIConfig;
import com.meizu.speaker.weixin.SHAEncryptor;

import net.sf.json.JSONObject;

/**
 * 
 * @author wangliao
 * 生成JsAPIConfig的工厂类
 */
public class WeixinFactory {
	private String accessTokenUrl;
	private HttpConnection httpConnection;
	private String ticketUrl;
	private String m_token;
	private String m_jsTicket;
	private String appId;
	
	public WeixinFactory(String appId,String accessTokenUrl,String ticketUrl){
		this.appId = appId;
		this.accessTokenUrl = accessTokenUrl;
		this.ticketUrl = ticketUrl;
		System.out.println("-accessTokenUrl: "+accessTokenUrl+"-ticketUrl:"+ticketUrl);
		this.httpConnection = new HttpConnection();
	}
	
	public WeixinFactory(String appId){
		this.appId = appId;
	}
	
	
	public String getM_token() {
		return m_token;
	}

	public void setM_token(String m_token) {
		this.m_token = m_token;
	}
	
	

	public String getM_jsTicket() {
		return m_jsTicket;
	}

	public void setM_jsTicket(String m_jsTicket) {
		this.m_jsTicket = m_jsTicket;
	}

	/**
	 * 从微信服务器获取token
	 * @return
	 * @throws Exception
	 */
	public String getTokenFromWechat() throws Exception{
		String responseText = httpConnection.get(accessTokenUrl);
		JSONObject obj = JSONObject.fromObject(responseText);
		String token  = obj.getString("access_token");
		//--在这里可以保存到缓存里
		setM_token(token);
		return token;	
	}
	
	/**
	 * 从微信服务器获取ticket卡券
	 * @return
	 * @throws Exception
	 */
	
	public  String getJsTicketFromWechat(String token) throws Exception{ 
		/*String token = getM_token();
		if(token != null){
		
			System.out.println("token不为空 "+token);
		}*/
		System.out.println("ticketUrl: "+ticketUrl+token);
        String responeText = httpConnection.get(ticketUrl+token);
        JSONObject obj = JSONObject.fromObject(responeText);
        String jsTicket = obj.getString("ticket");
        setM_jsTicket(jsTicket);
        return jsTicket;  
    }
	
	
	public JsAPIConfig createConfig(String link) throws Exception {
        JsAPIConfig config = new JsAPIConfig();
        config.setLink(link);
        String nonce = UUID.randomUUID().toString();
        String timestamp = Long.toString(System.currentTimeMillis() / 1000);
        String src = "jsapi_ticket=" + getM_jsTicket() + "&noncestr="
                + nonce + "&timestamp=" + timestamp + "&url="
                + config.getLink();
        System.out.println("src: "+src);
        String signature = SHAEncryptor.sha1(src);
        System.out.println("signature是 "+ signature);
        config.setAppId(appId);
        config.setDebug(true);
        config.setNonce(nonce);
        config.setTimestamp(timestamp);
        config.setSignature(signature);
        return config;
    }
	
	
	
	
	
	

}
