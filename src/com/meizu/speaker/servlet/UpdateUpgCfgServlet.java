package com.meizu.speaker.servlet;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.meizu.speaker.utils.ServletUtils;
import com.meizu.speaker.utils.UpgCfgHelper;

public class UpdateUpgCfgServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req,resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//接收前端请求的内容
		//待userid和pass，显示updateCfg。jsp页面
		//带upGradeJson,跳转success.jsp
		String upgCfgInfo = req.getParameter("upgCfgInfo");
		String userid = req.getParameter("userid");
		String pass = req.getParameter("pass");
		String release = req.getParameter("release");
		int releaseType = 0;
		if(release != null) {
			releaseType = Integer.parseInt(release);
		}
		if(userid != null && pass != null) {
			//进入过滤逻辑
			if(!"meizuSpeaker".equals(userid) || !"abc1234567890CBA".equals(pass)){
				ServletUtils.attachJsonResultToResponse(resp, "用户名或密码错误", req.getParameter("jsoncallback"));
				return;
			}
		} else {
			ServletUtils.attachJsonResultToResponse(resp, "用户名或密码错误", req.getParameter("jsoncallback"));
			return;
		}
		
		if(upgCfgInfo != null){
			if(UpgCfgHelper.getInstance().setUpgCfgInfo(upgCfgInfo, releaseType)) {
				req.setAttribute("info", upgCfgInfo);
			} else {
				ServletUtils.attachJsonResultToResponse(resp, "保存版本信息出错！", req.getParameter("jsoncallback"));
				return;
			}
		}else{
			ServletUtils.attachJsonResultToResponse(resp, "提交的版本信息不正确！", req.getParameter("jsoncallback"));
			return;
		}
		req.getRequestDispatcher("unaccessible/updateUpgCfgSuccess.jsp").forward(req, resp);
	}
}
