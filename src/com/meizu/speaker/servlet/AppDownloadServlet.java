package com.meizu.speaker.servlet;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Timer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.meizu.speaker.factory.WeixinFactory;
import com.meizu.speaker.weixin.FetchToken;
import com.meizu.speaker.weixin.JsAPIConfig;

/**
 * 处理前端提交的请求，实例化Config对象
 * @author wangliao
 *
 */
public class AppDownloadServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {	
		try {
			Enumeration typestr = req.getHeaderNames();   
			String userAgent = req.getHeader("user-agent").toLowerCase();  
			if(userAgent.contains("micromessenger")){
				RequestDispatcher rd = req.getRequestDispatcher("/appdownload.html");
				rd.forward(req, resp);
			} else {
				if(userAgent.contains("android")) {  
				    System.out.println("Android移动客户端"); 
				    resp.sendRedirect("http://lifekit-apk.b0.upaiyun.com/apk/LifeKit_A8.apk");//"http://lifekit.meizu.com/m/about/index.html"
				} else if(userAgent.contains("iphone") || userAgent.contains("ipad")) {  
				    System.out.println("iphone移动客户端");  
//				    resp.setCharacterEncoding("UTF-8");
//					resp.setContentType("text/html;charset=UTF-8");
//				    OutputStream out = resp.getOutputStream();	
//				    out.write("Ios客户端还没有发布，敬请期待！".getBytes("UTF-8"));
				    RequestDispatcher rd = req.getRequestDispatcher("/iosappdownload.html");
					rd.forward(req, resp);
				}  else {
				    System.out.println("其他客户端");  
				    RequestDispatcher rd = req.getRequestDispatcher("/appdownload.html");
					rd.forward(req, resp);
				}  
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			resp.flushBuffer();
		}
		//WeixinFactory factory = new WeixinFactory(appId,accessTokenUrl, ticketUrl);
		//WeixinFactory factory = new WeixinFactory(appId);
		//先获取token
		/*try {
			//String accessToken = factory.getTokenFromWechat();
			//System.out.println("token是"+ accessToken);
			//在获取ticket
			//String tiket = factory.getJsTicketFromWechat();
			//System.out.println("ticket是 "+ tiket);
			//构造Config 对象
			//JsAPIConfig cfg = factory.createConfig(link);
			if(cfg != null){
				//将cfg发送到前端页，使用jstl
				req.setAttribute("config", cfg);
				req.getRequestDispatcher("connect.jsp").forward(req,resp);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req,resp);
	}
	

}
