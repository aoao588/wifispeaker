package com.meizu.speaker.servlet;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.meizu.speaker.utils.ServletUtils;
import com.meizu.speaker.utils.UpgCfgHelper;

/**
 * Servlet implementation class getCfgServlet
 */
@WebServlet("/getCfgServlet")
public class GetUpgCfgServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//添加用户名和密码
		String userid = request.getParameter("userid");
		String pass = request.getParameter("pass");
		String release = request.getParameter("release");
		int releaseType = 0;
		if(release != null) {
			releaseType = Integer.parseInt(release);
		}
		if("meizuSpeaker".equals(userid) && "abc1234567890CBA".equals(pass)){
			String cfgInfo = UpgCfgHelper.getInstance().getUpgCfgInfo(releaseType);
			//发送给前端
			if(cfgInfo == null || cfgInfo.length() <= 0){
				ServletUtils.attachJsonResultToResponse(response, "没有可用的升级配置信息！", request.getParameter("jsoncallback"));

			}else{
				ServletUtils.attachJsonResultToResponse(response, cfgInfo, request.getParameter("jsoncallback"));
			}
		}else{
			ServletUtils.attachJsonResultToResponse(response, "用户名或者密码错误！",request.getParameter("jsoncallback"));
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}

}
