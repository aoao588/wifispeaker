package com.meizu.speaker.servlet;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.meizu.speaker.utils.ServletUtils;

public class SetUpgCfgServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req,resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//接收前端请求的内容
		//待userid和pass，显示updateCfg。jsp页面
		String userid = req.getParameter("userid");
		String pass = req.getParameter("pass");
		if(userid != null && pass != null) {
			//进入过滤逻辑
			if("meizuSpeaker".equals(userid) && "abc1234567890CBA".equals(pass)){
				//验证通过
				req.getRequestDispatcher("unaccessible/updateUpgCfg.jsp").forward(req, resp);
			}else{
				ServletUtils.attachJsonResultToResponse(resp, "用户名或密码错误", req.getParameter("jsoncallback"));
				return;
			}
		} else {
			ServletUtils.attachJsonResultToResponse(resp, "用户名或密码错误", req.getParameter("jsoncallback"));
			return;
		}
	}
}
