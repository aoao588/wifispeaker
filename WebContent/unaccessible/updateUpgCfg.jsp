<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Cache-Control" content="no-siteapp" />
		<script type="text/javascript">
			function toVaild(){
				var val = document.getElementById("upgCfgInfo_textarea").value;
				if(val.length > 0){
					return true;
				} else{
					alert("版本信息不能为空！");
					return false;
				}
			}
		</script>
	</head>
	<body>
		<form action="UpdateUpgCfg" method="post" onsubmit="return toVaild()">
			<table align="center" cellspace="0">
			    <tbody>
				    <tr align="left">
						<td>
							<text style="font-size:150%">请在这里输入升级的版本信息json串：</text>
						</td>
					</tr>
					<tr>
				        <td>&nbsp;</td>
				    </tr>
					<tr align="center">
						<td>
							<textarea width="100%" id="upgCfgInfo_textarea" rows="20" cols="80" name="upgCfgInfo"></textarea>
						</td>
					</tr>
					<tr align="center">
						<td>
							<input type="text" name="userid" value="meizuSpeaker" style="display:none"/>
						</td>
					</tr>
					<tr align="center">
						<td>
							<input type="text" name="pass" value="abc1234567890CBA"  style="display:none"/>
						</td>
					</tr>
					<tr>
				        <td>&nbsp;</td>
				    </tr>
					<tr align="center">
						<td>
							<div>
								<input type="radio" name="release" value="0" checked="checked"/>内测版
								&nbsp;
								&nbsp;
								<input type="radio" name="release" value="1"/>稳定版
							</div>
						</td>
					</tr>
					<tr>
				        <td>&nbsp;</td>
				    </tr>
					<tr align="center" colspan="2" >
						<td>
							<input type="submit" value="Submit" style="font-size:150%"/>
						</td>
					</tr>
			    </tbody>
			</table>			
		</form>
	</body>
</html>