<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/"+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="format-detection" content="telephone=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
</head>
<body>
<div class="page">
    <div class="weui_msg">
        <div class="weui_icon_area"><i class="weui_icon_success weui_icon_msg" id="image"></i></div>
        <div class="weui_text_area">
            <h2 class="weui_msg_title">配网界面</h2>
            <p id="msg_p" class="weui_msg_desc">稍等片刻，马上进入配网页面</p>
        </div>
        <div class="weui_opr_area" id="message">  
        </div>
        <div class="weui_extra_area">
            <a href="javacript:;" id="showDialog2">查看详情</a>
        </div>
         <!--BEGIN dialog2-->
        <div class="weui_dialog_alert" id="dialog2" style="display: none;">
        <div class="weui_mask"></div>
        <div class="weui_dialog">
            <div class="weui_dialog_hd"><strong class="weui_dialog_title">配网详情</strong></div>
            <div class="weui_dialog_bd">多次尝试都无法配置成功?<br>
            1.请检查输入的密码是否正确;<br>2.请重启路由器配网需要30秒</div>
            <div class="weui_dialog_ft">
                <a href="javascript:;" class="weui_btn_dialog primary">确定</a>
            </div>
        </div>
         <!--END dialog2-->
    </div>
    </div>
</div>
</body>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
    wx.config({
        beta : true, // 开启内测接口调用，注入wx.invoke方法
        debug : false, // 开启调试模式
        appId : '${config.appId}', // 第三方app唯一标识
        timestamp : '${config.timestamp}', // 生成签名的时间戳
        nonceStr : '${config.nonce}', // 生成签名的随机串
        signature : '${config.signature}',// 签名
        jsApiList : ['configWXDeviceWiFi'] // 需要使用的jsapi列表
    });

    var second = 5;
    wx.ready(function () {
             wx.invoke('configWXDeviceWiFi', {}, function(res){
                    //var err_msg = res.err_msg;
                    if(res.err_msg == 'configWXDeviceWiFi:ok') {
                        $('#message').html("配置 WIFI成功");
                        setInterval(count,1000);
                        return;
                    } else if(res.err_msg == 'configWXDeviceWiFi:fail') {
                    	$('#message').html("配网超时，没有收到智能设备的相关信息！");
                    	$('#image').removeClass().addClass("weui_icon_warn weui_icon_msg");
                    	$('#msg_p').text("目前无法配置成功，请稍后再试");
                    	
                    }else if(res.err_msg == 'configWXDeviceWiFi:cancel'){
                    	$('#message').html("已取消配网");
                    }
                });
    });

    function count(){
        second--;
        $('#second').html(second);
        if(second == 0){
        }
    }
</script>
</html>